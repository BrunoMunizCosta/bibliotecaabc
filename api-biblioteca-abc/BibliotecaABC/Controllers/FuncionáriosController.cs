﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BibliotecaABC.Dados;
using BibliotecaABC.Models;

namespace BibliotecaABC.Controllers
{
    [Route("api/biblioteca/[controller]")]
    [ApiController]
    public class FuncionáriosController : ControllerBase
    {
        private readonly BibliotecaABCContext _context;

        public FuncionáriosController(BibliotecaABCContext context)
        {
            _context = context;
        }

        // GET: api/Funcionários
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Funcionário>>> GetFuncionários()
        {
            return await _context.Funcionários.ToListAsync();
        }

        // GET: api/Funcionários/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Funcionário>> GetFuncionário(int id)
        {
            var funcionário = await _context.Funcionários.FindAsync(id);

            if (funcionário == null)
            {
                return NotFound();
            }

            return funcionário;
        }

        // PUT: api/Funcionários/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFuncionário(int id, Funcionário funcionário)
        {
            if (id != funcionário.Id)
            {
                return BadRequest();
            }

            _context.Entry(funcionário).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FuncionárioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Funcionários
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Funcionário>> PostFuncionário(Funcionário funcionário)
        {
            _context.Funcionários.Add(funcionário);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFuncionário", new { id = funcionário.Id }, funcionário);
        }

        // DELETE: api/Funcionários/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Funcionário>> DeleteFuncionário(int id)
        {
            var funcionário = await _context.Funcionários.FindAsync(id);
            if (funcionário == null)
            {
                return NotFound();
            }

            _context.Funcionários.Remove(funcionário);
            await _context.SaveChangesAsync();

            return funcionário;
        }

        private bool FuncionárioExists(int id)
        {
            return _context.Funcionários.Any(e => e.Id == id);
        }
    }
}
