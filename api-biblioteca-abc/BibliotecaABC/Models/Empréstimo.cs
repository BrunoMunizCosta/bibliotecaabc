﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BibliotecaABC.Models
{
    public class Empréstimo
    {
        public int AlunoId { get; set; }
        public int FuncionarioId { get; set; }
        public DateTime DataEmprestimo { get; set; }
        public DateTime DataDevolucao { get; set; }
        public bool Multa { get; set; }

        public Aluno Aluno { get; set; }
        public Funcionário Funcionário { get; set; }
        public Exemplar Exemplar { get; set; }
    }
}
