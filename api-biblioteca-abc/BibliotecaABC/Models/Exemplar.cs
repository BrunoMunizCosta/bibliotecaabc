﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BibliotecaABC.Models
{
    public class Exemplar
    {
        public int Id { get; set; }
        public int LivroId { get; set; }
        public string Nome { get; set; }
        public bool Emprestado { get; set; }

        public List<Empréstimo> Empréstimos { get; set; }
        public Livro Livro { get; set;}
    }
}
