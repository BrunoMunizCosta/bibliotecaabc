﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BibliotecaABC.Models;
using Microsoft.EntityFrameworkCore;

namespace BibliotecaABC.Dados
{
    public class BibliotecaABCContext : DbContext
    {
        public BibliotecaABCContext(DbContextOptions<BibliotecaABCContext> options) : base (options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Empréstimo>()
                .HasKey(ac => new { ac.AlunoId, ac.FuncionarioId, ac.DataEmprestimo});
        }

        public DbSet<Aluno> Alunos { get; set; }
        public DbSet<Funcionário> Funcionários { get; set; }
        public DbSet<Exemplar> Exemplares { get; set; }
        public DbSet<Livro> Livros { get; set; }
        public DbSet<BibliotecaABC.Models.Empréstimo> Empréstimo { get; set; }

    }
}
