﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BibliotecaABC.Dados;
using BibliotecaABC.Models;

namespace BibliotecaABC.Controllers
{
    [Route("api/biblioteca/[controller]")]
    [ApiController]
    public class ExemplaresController : ControllerBase
    {
        private readonly BibliotecaABCContext _context;

        public ExemplaresController(BibliotecaABCContext context)
        {
            _context = context;
        }

        // GET: api/Exemplares
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Exemplar>>> GetExemplares()
        {
            return await _context.Exemplares.ToListAsync();
        }

        // GET: api/Exemplares/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Exemplar>> GetExemplar(int id)
        {
            var exemplar = await _context.Exemplares.FindAsync(id);

            if (exemplar == null)
            {
                return NotFound();
            }

            return exemplar;
        }

        // PUT: api/Exemplares/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutExemplar(int id, Exemplar exemplar)
        {
            if (id != exemplar.Id)
            {
                return BadRequest();
            }

            _context.Entry(exemplar).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExemplarExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Exemplares
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Exemplar>> PostExemplar(Exemplar exemplar)
        {
            _context.Exemplares.Add(exemplar);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetExemplar", new { id = exemplar.Id }, exemplar);
        }

        // DELETE: api/Exemplares/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Exemplar>> DeleteExemplar(int id)
        {
            var exemplar = await _context.Exemplares.FindAsync(id);
            if (exemplar == null)
            {
                return NotFound();
            }

            _context.Exemplares.Remove(exemplar);
            await _context.SaveChangesAsync();

            return exemplar;
        }

        private bool ExemplarExists(int id)
        {
            return _context.Exemplares.Any(e => e.Id == id);
        }
    }
}
