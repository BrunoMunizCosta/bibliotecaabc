﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BibliotecaABC.Dados;
using BibliotecaABC.Models;

namespace BibliotecaABC.Controllers
{
    [Route("api/biblioteca/[controller]")]
    [ApiController]
    public class EmpréstimosController : ControllerBase
    {
        private readonly BibliotecaABCContext _context;

        public EmpréstimosController(BibliotecaABCContext context)
        {
            _context = context;
        }

        // GET: api/Empréstimos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Empréstimo>>> GetEmpréstimo()
        {
            return await _context.Empréstimo.ToListAsync();
        }

        // GET: api/Empréstimos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Empréstimo>> GetEmpréstimo(int id)
        {
            var empréstimo = await _context.Empréstimo.FindAsync(id);

            if (empréstimo == null)
            {
                return NotFound();
            }

            return empréstimo;
        }

        // PUT: api/Empréstimos/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmpréstimo(int id, Empréstimo empréstimo)
        {
            if (id != empréstimo.AlunoId)
            {
                return BadRequest();
            }

            _context.Entry(empréstimo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmpréstimoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Empréstimos
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Empréstimo>> PostEmpréstimo(Empréstimo empréstimo)
        {
            _context.Empréstimo.Add(empréstimo);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (EmpréstimoExists(empréstimo.AlunoId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetEmpréstimo", new { id = empréstimo.AlunoId }, empréstimo);
        }

        // DELETE: api/Empréstimos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Empréstimo>> DeleteEmpréstimo(int id)
        {
            var empréstimo = await _context.Empréstimo.FindAsync(id);
            if (empréstimo == null)
            {
                return NotFound();
            }

            _context.Empréstimo.Remove(empréstimo);
            await _context.SaveChangesAsync();

            return empréstimo;
        }

        private bool EmpréstimoExists(int id)
        {
            return _context.Empréstimo.Any(e => e.AlunoId == id);
        }
    }
}
