﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BibliotecaABC.Models
{
    public class Livro
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Autor { get; set; }
        public int Edicao { get; set; }
        public int Volume { get; set; }
        public string Editora { get; set; }
        public string Area { get; set; }
        public DateTime AnoPublicacao { get; set; }

        public List<Exemplar> Exemplares { get; set; }
    }
}