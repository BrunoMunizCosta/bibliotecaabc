
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';
import { Navbar, NavItem, NavDropdown, MenuItem, Nav } from 'react-bootstrap';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom'
import { Home } from './components/Home';
import { Alunos } from './components/Alunos';
import { Exemplares } from './components/Exemplares';
import { Livros } from './components/Livros';
import './index.css';

function App() {
  return (
    <Container className="Container">
      <BrowserRouter  id="nav">
        <Navbar bg='light' expand='lg'>
          <Navbar.Brand as={Link} to="/">Biblioteca ABC</Navbar.Brand>
          <Navbar.Toggle aria-controls='basic-navbar-nav' />
          <Navbar.Collapse id='basic-navbar-nav'>
            <Nav className='mr-auto'>
              <Nav.Link as={Link} to="/">Home</Nav.Link>
              <NavDropdown title='Registros' id='basic-nav-dropdown'>
                <NavDropdown.Item as={Link} to='/alunos'>Alunos</NavDropdown.Item>
                <NavDropdown.Item as={Link} to='/exemplares'>Exemplares</NavDropdown.Item>
                <NavDropdown.Item as={Link} to='/livros'>Livros</NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Switch>
          <Route path="/" exact={true} component={Home} />
          <Route path="/alunos" component={Alunos} />
          <Route path="/exemplares" component={Exemplares} />
          <Route path="/livros" component={Livros} />
        </Switch>
      </BrowserRouter>
    </Container>
  );
}

export default App;
