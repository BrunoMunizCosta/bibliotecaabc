import React, { Component } from 'react';
import { Button, Form, Modal, Row, Table } from 'react-bootstrap';

export class Exemplares extends Component {

  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      livroId: 0,
      nome: '',
      emprestado: false,
      exemplares: [],
      modalAberta: false
    };

    this.buscarExemplares = this.buscarExemplares.bind(this);
    this.buscarExemplar = this.buscarExemplar.bind(this);
    this.inserirExemplar = this.inserirExemplar.bind(this);
    this.atualizarExemplar = this.atualizarExemplar.bind(this);
    this.excluirExemplar = this.excluirExemplar.bind(this);
    this.renderTabela = this.renderTabela.bind(this);
    this.abrirModalInserir = this.abrirModalInserir.bind(this);
    this.fecharModal = this.fecharModal.bind(this);
    this.atualizaLivroId = this.atualizaLivroId.bind(this);
    this.atualizaNome = this.atualizaNome.bind(this);
    this.atualizaEmprestado = this.atualizaEmprestado.bind(this);
  }

  componentDidMount() {
    this.buscarExemplares();
  }

  // GET (todos exemplares)
  buscarExemplares() {
    fetch('https://localhost:44348/api/biblioteca/exemplares')
      .then(response => response.json())
      .then(data => this.setState({ exemplares: data }));
  }
  
  //GET (exempalr com determinado id)
  buscarExemplar(id) {
    fetch('https://localhost:44348/api/biblioteca/exemplares/' + id)
      .then(response => response.json())
      .then(data => this.setState(
        {
          id: data.id,
          livroid: data.livroId,
          nome: data.nome,
          emprestado: data.emprestado,
        }));
  }

  inserirExemplar = (exemplar) => {
    fetch('https://localhost:44348/api/biblioteca/exemplares', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(exemplar)
    }).then((resposta) => {

      if (resposta.ok) {
        this.buscarExemplares();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    }).catch(console.log);
  }

  atualizarExemplar(exemplar) {
    fetch('https://localhost:44348/api/biblioteca/exemplares/' + exemplar.id, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(exemplar)
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarExemplares();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  excluirExemplar = (id) => {
    fetch('https://localhost:44348/api/biblioteca/exemplares/' + id, {
      method: 'DELETE',
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarExemplares();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  atualizaLivroId(e) {
    this.setState({
      livroId: e.target.value
    });
  }

  atualizaNome(e) {
    this.setState({
      nome: e.target.value
    });
  }

  atualizaEmprestado(e) {
    this.setState({
      emprestado: e.target.value
    });
  }

  abrirModalInserir() {
    this.setState({
      modalAberta: true
    })
  }

  abrirModalAtualizar(id) {
    this.setState({
      id: id,
      modalAberta: true
    });

    this.buscarExemplar(id);
  }

  fecharModal() {
    this.setState({
      id: 0,
      livroId: 0,
      nome: "",
      emprestado: false,
      modalAberta: false
    })
  }

  submit = () => {
    const exemplar = {
      id: this.state.id,
      livroId: this.state.livroId,
      nome: this.state.nome,
      emprestado: this.state.emprestado,
    };

    if (this.state.id === 0) {
      this.inserirExemplar(exemplar);
    } else {
      this.atualizarExemplar(exemplar);
    }
  }

  renderModal() {
    return (
      <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
        <Modal.Header closeButton>
          <Modal.Title>Preencha os dados do exemplar</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id="modalForm" onSubmit={this.submit}>
          <Form.Group>
              <Form.Label>Livro ID</Form.Label>
              <Form.Control type='text' placeholder='ID do Livro' value={this.state.livroId} onChange={this.atualizaLivroId} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Nome</Form.Label>
              <Form.Control type='text' placeholder='Nome do Exemplar' value={this.state.nome} onChange={this.atualizaNome} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Emprestado</Form.Label>
              <Form.Control type='bool' placeholder='Emprestado' value={this.state.emprestado} onChange={this.atualizaEmprestado} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.fecharModal}>
            Cancelar
      </Button>
          <Button variant="primary" form="modalForm" type="submit">
            Confirmar
      </Button>
        </Modal.Footer>
      </Modal>
    );
  }

  renderTitulo() {
    return (
      <div id="div-row">
        <Row>
          <h1>Registros de Exemplares</h1>
          <Button id="btnAdd" variant="primary" className="button-novo" onClick={this.abrirModalInserir}>Registrar Exemplar</Button>
        </Row>
      </div>
    )
  }

  renderTabela() {
    return (
      <div id="div-table">
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Livro ID</th>
            <th>Nome</th>
            <th>Emprestado</th>
            <th>Opções</th>
          </tr>
        </thead>
        <tbody>
          {this.state.exemplares.map((exemplar) => (
            <tr key={exemplar.id}>
              <td>{exemplar.livroId}</td>
              <td>{exemplar.nome}</td>
              <td>{exemplar.emprestado}</td>
              <td>
                <div>
                  <Button variant="link" onClick={() => this.abrirModalAtualizar(exemplar.id)}>Atualizar</Button>
                  <Button variant="link" onClick={() => this.excluirAluno(exemplar.id)}>Excluir</Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      </div>
    );
  }

  render() {
    return (
      <div id="div-cont">
      <br />
      {this.renderTitulo()}
        {this.renderTabela()}
        {this.renderModal()}
      </div>
    );
  }
}