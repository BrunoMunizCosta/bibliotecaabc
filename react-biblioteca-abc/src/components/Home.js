import React, { Component } from 'react';
import imagem from './306638.jpg';

export class Home extends Component {
  render() {
    return (
        
      <div id="div-contHome">

        <div id="div-row">
        <h1>Bem vindo à aplicação Biblioteca ABC</h1>
        <p>A BibliotecaABC foi desenvolvida durante a disciplina Mobile Web Development utilizando as seguintes tecnologias:</p>
        <ul>
          <li>.Net Core Web API</li>
          <li>PostgreSQL</li>
          <li>ReactJS</li>
        </ul>
        </div>
      </div>
    );
  }
}
