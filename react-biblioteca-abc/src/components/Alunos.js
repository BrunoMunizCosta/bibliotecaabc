import React, { Component } from 'react';
import { Button, Form, Modal, Row, Table } from 'react-bootstrap';

export class Alunos extends Component {

  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      nome: '',
      email: '',
      telefone: 0,
      alunos: [],
      modalAberta: false
    };

    this.buscarAlunos = this.buscarAlunos.bind(this);
    this.buscarAluno = this.buscarAluno.bind(this);
    this.inserirAluno = this.inserirAluno.bind(this);
    this.atualizarAluno = this.atualizarAluno.bind(this);
    this.excluirAluno = this.excluirAluno.bind(this);
    this.renderTabela = this.renderTabela.bind(this);
    this.abrirModalInserir = this.abrirModalInserir.bind(this);
    this.fecharModal = this.fecharModal.bind(this);
    this.atualizaId = this.atualizaId.bind(this);
    this.atualizaNome = this.atualizaNome.bind(this);
    this.atualizaEmail = this.atualizaEmail.bind(this);
    this.atualizaTelefone = this.atualizaTelefone.bind(this);
  }

  componentDidMount() {
    this.buscarAlunos();
  }

  // GET (todos alunos)
  buscarAlunos() {
    fetch('https://localhost:44348/api/biblioteca/alunos')
      .then(response => response.json())
      .then(data => this.setState({ alunos: data }));
  }

  //GET (aluno com determinado id)
  buscarAluno(id) {
    fetch('https://localhost:44348/api/biblioteca/alunos/' + id)
      .then(response => response.json())
      .then(data => this.setState(
        {
          id: data.id,
          nome: data.nome,
          email: data.email,
          telefone: data.telefone,
        }));
  }

  inserirAluno = (aluno) => {
    fetch('https://localhost:44348/api/biblioteca/alunos', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(aluno)
    }).then((resposta) => {

      if (resposta.ok) {
        this.buscarAlunos();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    }).catch(console.log);
  }

  atualizarAluno(aluno) {
    fetch('https://localhost:44348/api/biblioteca/alunos/' + aluno.id, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(aluno)
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarAlunos();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  excluirAluno = (id) => {
    fetch('https://localhost:44348/api/biblioteca/alunos/' + id, {
      method: 'DELETE',
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarAlunos();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  atualizaId(e) {
    this.setState({
      id: e.target.value
    });
  }

  atualizaNome(e) {
    this.setState({
      nome: e.target.value
    });
  }

  atualizaEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  atualizaTelefone(e) {
    this.setState({
      telefone: e.target.value
    });
  }

  abrirModalInserir() {
    this.setState({
      modalAberta: true
    })
  }

  abrirModalAtualizar(id) {
    this.setState({
      id: id,
      modalAberta: true
    });

    this.buscarAluno(id);
  }

  fecharModal() {
    this.setState({
      id: 0,
      nome: "",
      email: "",
      telefone: 0,
      modalAberta: false
    })
  }

  submit = () => {
    const aluno = {
      id: this.state.id,
      nome: this.state.nome,
      email: this.state.email,
      telefone: this.state.telefone,
    };

    if (this.state.id === 0) {
      this.inserirAluno(aluno);
    } else {
      this.atualizarAluno(aluno);
    }
  }

  renderModal() {
    return (
      <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
        <Modal.Header closeButton>
          <Modal.Title>Preencha os dados do aluno</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id="modalForm" onSubmit={this.submit}>
            <Form.Group>
              <Form.Label>ID</Form.Label>
              <Form.Control type='number' placeholder='ID do Aluno' value={this.state.id} onChange={this.atualizaId} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Nome</Form.Label>
              <Form.Control type='text' placeholder='Nome do Aluno' value={this.state.nome} onChange={this.atualizaNome} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control type='email' placeholder='Email do Aluno' value={this.state.email} onChange={this.atualizaEmail} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Telefone</Form.Label>
              <Form.Control type='number' placeholder='Telefone do Aluno' value={this.state.telefone} onChange={this.atualizaTelefone} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.fecharModal}>
            Cancelar
      </Button>
          <Button variant="primary" form="modalForm" type="submit">
            Confirmar
      </Button>
        </Modal.Footer>
      </Modal>
    );
  }
 
  renderTitulo() {
    return (
      <div id="div-row">
        <Row>
          <h1>Registros de Alunos</h1>
          <Button id="btnAdd" variant="primary" className="button-novo" onClick={this.abrirModalInserir}>Adicionar Aluno</Button>
        </Row>
      </div>
    )
  }

  renderTabela() {
    return (
        <div id="div-table">
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Nome</th>
                <th>Email</th>
                <th>Telefone</th>
                <th>Opções</th>
              </tr>
            </thead>
            <tbody>
              {this.state.alunos.map((aluno) => (
                <tr key={aluno.id}>
                  <td>{aluno.nome}</td>
                  <td>{aluno.email}</td>
                  <td>{aluno.telefone}</td>
                  <td>
                    <div>
                      <Button variant="link" onClick={() => this.abrirModalAtualizar(aluno.id)}>Atualizar</Button>
                      <Button variant="link" onClick={() => this.excluirAluno(aluno.id)}>Excluir</Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
    );
  }

  render() {
    return (
      <div id="div-cont">
        <br />
        {this.renderTitulo()}
        {this.renderTabela()}
        {this.renderModal()}
      </div>
    );
  }
}