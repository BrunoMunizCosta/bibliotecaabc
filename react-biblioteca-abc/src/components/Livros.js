import React, { Component } from 'react';
import { Button, Form, Modal, Row, Table } from 'react-bootstrap';

export class Livros extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: 0,
            nome: '',
            autor: '',
            edicao: 0,
            volume: 0,
            editora: '',
            area: '',
            anoPublicacao: '',
            livros: [],
            modalAberta: false
        };

        this.buscarLivros = this.buscarLivros.bind(this);
        this.buscarLivro = this.buscarLivro.bind(this);
        this.inserirLivro = this.inserirLivro.bind(this);
        this.atualizarLivro = this.atualizarLivro.bind(this);
        this.excluirLivro = this.excluirLivro.bind(this);
        this.renderTabela = this.renderTabela.bind(this);
        this.abrirModalInserir = this.abrirModalInserir.bind(this);
        this.fecharModal = this.fecharModal.bind(this);
        this.atualizaNome = this.atualizaNome.bind(this);
        this.atualizaAutor = this.atualizaAutor.bind(this);
        this.atualizaEdicao = this.atualizaEdicao.bind(this);
        this.atualizaVolume = this.atualizaVolume.bind(this);
        this.atualizaEditora = this.atualizaEditora.bind(this);
        this.atualizaArea = this.atualizaArea.bind(this);
        this.atualizaAnoPublicacao = this.atualizaAnoPublicacao.bind(this);
    }

    componentDidMount() {
        this.buscarLivros();
    }
    // GET (todos livros)
    buscarLivros() {
        fetch('https://localhost:44348/api/biblioteca/livros')
            .then(response => response.json())
            .then(data => this.setState({ livros: data }));
    }

    //GET (livro com determinado id)
    buscarLivro(id) {
        fetch('https://localhost:44348/api/biblioteca/livros/' + id)
            .then(response => response.json())
            .then(data => this.setState(
                {
                    id: data.id,
                    nome: data.nome,
                    autor: data.autor,
                    edicao: data.edicao,
                    volume: data.volume,
                    editora: data.editora,
                    area: data.area,
                    anoPublicacao: data.anoPublicacao,
                }));
    }

    inserirLivro = (livro) => {
        fetch('https://localhost:44348/api/biblioteca/livros', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(livro)
        }).then((resposta) => {

            if (resposta.ok) {
                this.buscarLivros();
                this.fecharModal();
            } else {
                alert(JSON.stringify(resposta));
            }
        }).catch(console.log);
    }

    atualizarLivro(livro) {
        fetch('https://localhost:44348/api/biblioteca/livros/' + livro.id, {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(livro)
        }).then((resposta) => {
            if (resposta.ok) {
                this.buscarLivros();
                this.fecharModal();
            } else {
                alert(JSON.stringify(resposta));
            }
        });
    }

    excluirLivro = (id) => {
        fetch('https://localhost:44348/api/biblioteca/livros/' + id, {
            method: 'DELETE',
        }).then((resposta) => {
            if (resposta.ok) {
                this.buscarLivros();
                this.fecharModal();
            } else {
                alert(JSON.stringify(resposta));
            }
        });
    }

    atualizaNome(e) {
        this.setState({
            nome: e.target.value
        });
    }

    atualizaAutor(e) {
        this.setState({
            autor: e.target.value
        });
    }

    atualizaEdicao(e) {
        this.setState({
            edicao: e.target.value
        });
    }

    atualizaVolume(e) {
        this.setState({
            volume: e.target.value
        });
    }

    atualizaEditora(e) {
        this.setState({
            editora: e.target.value
        });
    }

    atualizaArea(e) {
        this.setState({
            area: e.target.value
        });
    }

    atualizaAnoPublicacao(e) {
        this.setState({
            anoPublicacao: e.target.value
        });
    }

    abrirModalInserir() {
        this.setState({
            modalAberta: true
        })
    }

    abrirModalAtualizar(id) {
        this.setState({
            id: id,
            modalAberta: true
        });

        this.buscarLivro(id);
    }

    fecharModal() {
        this.setState({
            id: 0,
            nome: '',
            autor: '',
            edicao: 0,
            volume: 0,
            editora: '',
            area: '',
            anoPublicacao: '',
            modalAberta: false
        })
    }

    submit = () => {
        const livro = {
            id: this.state.id,
            nome: this.state.nome,
            autor: this.state.autor,
            edicao: this.state.edicao,
            volume: this.state.volume,
            editora: this.state.editora,
            area: this.state.area,
            anoPublicacao: this.state.anoPublicacao,
        };

        if (this.state.id === 0) {
            this.inserirLivro(livro);
        } else {
            this.atualizarLivro(livro);
        }
    }

    renderModal() {
        return (
            <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Preencha os dados do livro</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form id="modalForm" onSubmit={this.submit}>
                        <Form.Group>
                            <Form.Label>Nome</Form.Label>
                            <Form.Control type='text' placeholder='Nome do Livro' value={this.state.nome} onChange={this.atualizaNome} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Autor</Form.Label>
                            <Form.Control type='text' placeholder='Autor do Livro' value={this.state.autor} onChange={this.atualizaAutor} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Edição</Form.Label>
                            <Form.Control type='text' placeholder='Edição do Livro' value={this.state.edicao} onChange={this.atualizaEdicao} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Volume</Form.Label>
                            <Form.Control type='text' placeholder='Volume do Livro' value={this.state.volume} onChange={this.atualizaVolume} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Editora</Form.Label>
                            <Form.Control type='text' placeholder='Editora do Livro' value={this.state.editora} onChange={this.atualizaEditora} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Área</Form.Label>
                            <Form.Control type='text' placeholder='Área do Livro' value={this.state.area} onChange={this.atualizaArea} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Ano de Publicação</Form.Label>
                            <Form.Control type='text' placeholder='Ano de Publicação do Livro' value={this.state.anoPublicacao} onChange={this.atualizaAnoPublicacao} />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={this.fecharModal}>
                        Cancelar
                    </Button>
                    <Button variant="primary" form="modalForm" type="submit">
                        Confirmar
                    </Button>
                </Modal.Footer>
            </Modal>
        );
    }

    renderTitulo() {
        return (
          <div id="div-row">
            <Row>
              <h1>Registros de Livros</h1>
              <Button id="btnAdd" variant="primary" className="button-novo" onClick={this.abrirModalInserir}>Registrar Livro</Button>
            </Row>
          </div>
        )
      }

    renderTabela() {
        return (
            <div id="div-table">
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Autor</th>
                        <th>Edição</th>
                        <th>Volume</th>
                        <th>Editora</th>
                        <th>Área</th>
                        <th>Ano de Publicação</th>
                        <th>Opções</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.livros.map((livro) => (
                        <tr key={livro.id}>
                            <td>{livro.nome}</td>
                            <td>{livro.autor}</td>
                            <td>{livro.edicao}</td>
                            <td>{livro.volume}</td>
                            <td>{livro.editora}</td>
                            <td>{livro.area}</td>
                            <td>{livro.anoPublicacao}</td>
                            <td>
                                <div>
                                    <Button variant="link" onClick={() => this.abrirModalAtualizar(livro.id)}>Atualizar</Button>
                                    <Button variant="link" onClick={() => this.excluirLivro(livro.id)}>Excluir</Button>
                                </div>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
            </div>
        );
    }

    render() {
        return (
            <div id="div-cont">
        <br />
        {this.renderTitulo()}
                {this.renderTabela()}
                {this.renderModal()}
            </div>
        );
    }
}